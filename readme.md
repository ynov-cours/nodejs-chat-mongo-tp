# TP MongoDB x NodeJS
###### Chat en ligne avec persistance de données
###### Réalisé par Enzo Avagliano - Ynov Info B3 - Spé Web


#### Utilisation du projet :
```bash
# Cloner le repo GitLab
git clone https://gitlab.com/ynov-cours/nodejs-chat-mongo-tp.git
```

```bash
# Installer les dépendances
npm i
```

```bash
# Démarrer le serveur
npm start
```

**Adresse par défaut du serveur : http://localhost:3000**
