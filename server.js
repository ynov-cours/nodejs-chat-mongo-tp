const express = require('express');
const app = express()
const http = require('http').createServer(app);
const io = require('socket.io')(http);
const { MongoClient } = require('mongodb');
const port = 3000;
const uri = 'mongodb+srv://user_dev:sIRaGIQwc6V8dMah@clusterchat.ea5lx.mongodb.net/chat_db?retryWrites=true&w=majority';
const client = new MongoClient(uri);
let connectedUsers = 0;
let messagesCount = 0;

app.use('/static', express.static('public'));

client.connect((err, db) => {
  if (err) {
    process.exit(1);
  } else {
    let dbo = db.db('chat_db');
    console.log("\n\x1b[1m\x1b[36m[Connection à la base de données réussie]\x1b[0m")
    connectedUsers = 0;
    let stockedMessages = [];

    app.get('/', (req, res) => {
      res.sendFile(__dirname + '/index.html');
    });

    http.listen(port, () => {
      console.log('=> Chat server ready\n');
      console.log('\x1b[1m=======> Starting server logs <=======\x1b[0m');
    });

    io.on ('connection', (socket) => {
      console.log(`Users: +1\tTotal users: ${connectedUsers +1}`); //Log de serveur

      // On envoie à l'user les messages déja stockés
      dbo.collection("messages").find({}, { projection: { _id: 0 } }).toArray(function(err, result) {
        if (err) throw err;

        messagesCount = result.length; // NOT WORKING...
        socket.emit('messages counter', messagesCount); // NOT WORKING...

        for (let i = 0; i < result.length; i++) {
          socket.emit('stocked messages', result[i].message);
        }

      });
      io.emit('chat message', `Un utilisateur s'est connecté`); // On annonce dans le chat qu'un utilisateur s'est connecté
      connectedUsers++;


      socket.on('disconnect', () => { // On annonce dans le chat qu'un utilisateur se deconnecte
        console.log(`Users: -1\tTotal users: ${connectedUsers -1}`);
        io.emit('chat message', `Un utilisateur s'est déconnecté`);
        connectedUsers--;
        countUsers(connectedUsers);

      });
      countUsers(connectedUsers);

      socket.on('chat message', (msg) => {
        io.emit('chat message', msg);
        let obj = {message: msg}
        dbo.collection('messages').insertOne(obj, function(err, res) {
          if (err) throw err;
          console.log("Message inserted")
        })
      });

      function countUsers(users) {
        io.emit('connectedUsers', connectedUsers)
      }
    });
  }
})
