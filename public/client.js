let socket = io();

let messagesCount = document.getElementById('sent-messages');
let messages = document.getElementById('messages');
let form = document.getElementById('messages-form');
let input = document.getElementById('input');
let connected = document.getElementById('online-users');



form.addEventListener('submit', function(e) {
  e.preventDefault();
  if (input.value) {
    socket.emit('chat message', input.value);
    input.value = '';
    messages.scrollTop = messages.scrollHeight;
  }
});

socket.on('chat message', function(msg) {
  let item = document.createElement('li');
  let current = new Date();
  item.innerHTML = `<small class="low-opacity">${current.toLocaleTimeString()}</small> ${msg}`;
  messages.appendChild(item);
});

socket.on('connectedUsers', function(connectedUsers) {
  connected.innerHTML = `Utilisateurs connectés : ${connectedUsers}`;
});

// NOT WORKING...
socket.on('messages counter', function(messagesCount){
  messagesCount.innerHTML = `Messages totaux : ${messagesCount}`;
});

socket.on('stocked messages', function(stocked) {
  console.log(stocked)
  let item = document.createElement('li');
  item.innerHTML = `<small class="low-opacity">Old</small> - ${stocked}`;
  messages.appendChild(item);
  messages.scrollTop = messages.scrollHeight;
});
